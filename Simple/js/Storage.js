var myStorage = {};


myStorage.indexedDB = {};

myStorage.indexedDB.onerror = function(e) {
	console.log(e);
};

myStorage.indexedDB.create = function() {
	var request = indexedDB.open("primeDB");
	request.onupgradeneeded = function (e) {
		var db = e.target.result;

		if (db.objectStoreNames.contains("prime_number")) {
			db.deleteObjectStore("prime_number");
		}

		var store = db.createObjectStore("prime_number", { keyPath : "id", autoIncrement : true });
		store.createIndex('idName', 'id');
	};

	request.onsuccess = function(e) {
		e.target.result.close();
        myStorage.indexedDB.filDB();
	};

	request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.filDB = function() {
    var request = indexedDB.open("primeDB");
    request.onsuccess = function(e) {
        var db = e.target.result;
        var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
        var store = trans.objectStore("prime_number");
        var keyRange = IDBKeyRange.lowerBound(0);
        var cursorRequest = store.openCursor(keyRange);
        var count = 0;
        cursorRequest.onsuccess = function(e) {
            var result = e.target.result;
            if(!!result == false){
                return;
            }
            ++count;
        };

        trans.oncomplete = function(e) {
            if(!count) {
                myStorage.indexedDB.addPrime(2);
                myStorage.indexedDB.addPrime(3);
                myStorage.indexedDB.addPrime(5);
                changeHtmlTextValue("num_count", 3);
            }
            db.close();
        };

        cursorRequest.onerror = myStorage.indexedDB.onerror;
    };
    request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.addPrime = function(primeNumber) {
	var request = indexedDB.open("primeDB");
	request.onsuccess = function(e) {
		var db = e.target.result;
		var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
		var store = trans.objectStore("prime_number");

		var data = {
			"number": primeNumber
		};

		var request = store.put(data);

		trans.oncomplete = function(e){
            var prev_val = parseInt(localStorage.getItem("prime_count") ? localStorage.getItem("prime_count") : 0);
            changeHtmlTextValue("prime_count", prev_val + 1);
			db.close();
		};

		request.onerror = function(e) {
			console.log("Error Adding: ", e);
		};
	};
	request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.deletePrime = function(id) {
	var request = indexedDB.open("primeDB");
	request.onsuccess = function(e) {
		var db = e.target.result;
		var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
		var store = trans.objectStore("prime_number");
		var request = store.delete(id);

		trans.oncomplete = function(e) {
			db.close();
		};

		request.onerror = function(e) {
			console.log("Error Adding: ", e);
		};
	};
	request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.getPrime = function(id) {
	var request = indexedDB.open("primeDB");
	request.onsuccess = function(e) {
		var db = e.target.result;
		var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_ONLY);
		var store = trans.objectStore("prime_number");

		var storeRequest = store.get(id);

		storeRequest.onsuccess = function(e) {
			showDetails(e.target.result);
		};
		
		trans.oncomplete = function(e) {
			db.close();
		};

		storeRequest.onerror = function(e) {
			console.log("Error Getting: ", e);
		};
	};
	request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.getAllPrimes = function() {
	var request = indexedDB.open("primeDB");
	request.onsuccess = function(e) {
		var db = e.target.result;		
		var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
		var store = trans.objectStore("prime_number");

		// Get everything in the store;
		var keyRange = IDBKeyRange.lowerBound(0);
		var cursorRequest = store.openCursor(keyRange);

		cursorRequest.onsuccess = function(e) {
			var result = e.target.result;
			if(!!result == false){
				return;
			}
		
			//renderTodo(result.value);
			result.continue();
		};
		
		trans.oncomplete = function(e) {
			db.close();
		};

		cursorRequest.onerror = myStorage.indexedDB.onerror;
	};
	request.onerror = myStorage.indexedDB.onerror;
};

myStorage.indexedDB.deleteDB = function (){
	var deleteRequest = indexedDB.deleteDatabase("primeDB");
    deleteRequest.onsuccess = function (e)
    {
		alert("deleted");
		myStorage.indexedDB.create();
    }
	deleteRequest.onblocked = function (e)
    {
		alert("blocked");
    }
	deleteRequest.onerror = myStorage.indexedDB.onerror;
};

myStorage.IDBTransactionModes = { "READ_ONLY": "readonly", "READ_WRITE": "readwrite", "VERSION_CHANGE": "versionchange" };