/**
 * Created by andryzonov on 26/01/15.
 */

function changeHtmlTextValue(key, newValue) {
    if (typeof(Storage) != "undefined") {
        localStorage.setItem(key, newValue);
        var storedElements = document.getElementsByClassName("updating_block");
        for (var i = 0; i < storedElements.length; i++) {
            if (key == storedElements[i].id){
                document.getElementById(storedElements[i].id).innerHTML = newValue;
            }
        }
    }
}

$(function() {
    var number = {};
    number.init = {};
    number.init.worker = {};
    number.init.localStorage = {};
    numPages = 10;
    var stop = true;

    var minutesLabel = document.getElementById("minutes");
    var secondsLabel = document.getElementById("seconds");
    var totalSeconds = 0;
    setInterval(setTime, 1000);

    function setTime()
    {
        if(!stop) {
            ++totalSeconds;
            changeHtmlTextValue("work_time",totalSeconds);
            secondsLabel.innerHTML = pad(totalSeconds % 60);
            minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
        }
    }

    function pad(val)
    {
        var valString = val + "";
        if (valString.length < 2) {
            return "0" + valString;
        }
        else {
            return valString;
        }
    }

    number.init.localStorage.init = function() {
        if (typeof(Storage) != "undefined") {
            var storedElements = document.getElementsByClassName("updating_block");
            for (var i = 0; i < storedElements.length; i++) {
                var id = storedElements[i].id;
                document.getElementById(id).innerHTML = localStorage.getItem(id) ? localStorage.getItem(id) : 0;
            }
            totalSeconds = parseInt(localStorage.getItem("work_time") ? localStorage.getItem("work_time") : 0);
            secondsLabel.innerHTML = pad(totalSeconds % 60);
            minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
        } else {
            $('#statstic').html('<h2 class="error_message"> Ваш браузер не поддерживает Web Storage </h2>');
        }

    };

    $('#toggle_search').click(function () {
        if (this.value != "Старт") {
            this.value = "Старт";
            stop = true;
            document.getElementById("state").innerHTML = "Не запущено";
        } else {
            this.value = "Стоп";
            document.getElementById("state").innerHTML = "Запущено";
            stop = false;
            var Step = 0;

            function isPrime(maxRevisionObject) {
                var num = maxRevisionObject.number ? maxRevisionObject.number : maxRevisionObject;
                if (Step > 3) {
                    num += 4;
                    Step = 0;
                } else {
                    num += 2;
                    Step++;
                }
                changeHtmlTextValue("num_count", num);
                number.init.worker.postMessage(num);
                number.init.worker.onmessage = function (e) {
                    if (stop)
                        return;
                    if (e.data) {
                        myStorage.indexedDB.addPrime(e.data);
                        isPrime(e.data);
                    } else {
                        //var prev_val = parseInt(localStorage.getItem("num_count") ? localStorage.getItem("num_count") : 0);
                        //changeHtmlTextValue("num_count", prev_val + 1);
                        isPrime(num);
                    }
                };
            }

            getMaxNumber(isPrime);
        }
    });

    $('#numbers').click(function () {
        var request = indexedDB.open("primeDB");
        request.onsuccess = function (e) {
            var db = e.target.result;
            var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
            var store = trans.objectStore("prime_number");

            var keyRange = IDBKeyRange.lowerBound(0);
            var cursorRequest = store.openCursor(keyRange);
            var count = 0;
            cursorRequest.onsuccess = function (e) {
                var result = e.target.result;
                if (!!result == false) {
                    return;
                }
                var num = result.value.number;
                showNumber(num);
                result.continue();
            };
            trans.oncomplete = function (e) {
                paginate();
                db.close();
            };
        }
    });

    function paginate(){
        new List('prime-list', {
            valueNames: ['name'],
            page: numPages,
            plugins: [ ListPagination({}) ]
        });
    }

    $('#draw_canvas').click(function () {
        getMaxNumber(draw);
        function draw(maxRevisionObject) {
            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext('2d'),
            cellSize = 1,
            objectsCount = maxRevisionObject.number,
            lenght = Math.sqrt(objectsCount).toFixed(0);
            canvas.width = lenght * cellSize;
            canvas.height = lenght * cellSize;
            ctx.fillStyle = '#000';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var request = indexedDB.open("primeDB");
            var empty = new Array();

            request.onsuccess = function (e) {
                var db = e.target.result;
                var trans = db.transaction(["prime_number"], myStorage.IDBTransactionModes.READ_WRITE);
                var store = trans.objectStore("prime_number");

                // Get everything in the store;
                var keyRange = IDBKeyRange.lowerBound(0);
                var cursorRequest = store.openCursor(keyRange);

                cursorRequest.onsuccess = function (e) {
                    var result = e.target.result;
                    if (!!result == false) {
                        return;
                    }
                    var num = result.value.number;
                    var add = false;
                    for (var j = empty.length; j < num; j++){
                        empty.push(0);
                        add = true;
                    }
                    if(add)
                        empty.push(1);
                    result.continue();
                };

                trans.oncomplete = function (e) {

                    for (var j = 0; j < lenght; j++)
                        for (var i = 0; i < lenght; i++) {
                            var numb = j*lenght + i;
                            switch (empty[numb]) {
                                case 1:
                                    DrawWhite(i, j);
                                    break;
                            }
                        }
                        function DrawWhite(x, y) {
                            ctx.fillStyle = "#FFFFFF";
                            ctx.fillRect(x*cellSize, y*cellSize, cellSize, cellSize);
                        };
                    db.close();
                };

                cursorRequest.onerror = myStorage.indexedDB.onerror;
            };
            request.onerror = myStorage.indexedDB.onerror;
        }
    });

    function getMaxNumber (callback) {
        var openReq = indexedDB.open("primeDB");
        openReq.onsuccess = function() {
            var db = openReq.result;
            var transaction = db.transaction("prime_number", 'readonly');
            var objectStore = transaction.objectStore("prime_number");
            var index = objectStore.index('idName');
            var openCursorRequest = index.openCursor(null, 'prev');
            var maxRevisionObject = null;

            openCursorRequest.onsuccess = function (event) {
                if (event.target.result) {
                    maxRevisionObject = event.target.result.value;
                }
            };
            transaction.oncomplete = function (event) {
                db.close();
                if(callback)
                    callback(maxRevisionObject);
            };
        }
    }

    function showNumber(prime_number, number_id) {
        $('ul.list').append('<li><p class="name">' + prime_number + '</p></li>');
    }

    function init() {
        if (window.indexedDB) {
            number.init.worker = new Worker('js/primeTest.js')
            myStorage.indexedDB.create();
            number.init.localStorage.init();
            var prev_val = parseInt(localStorage.getItem("app_runs_count") ? localStorage.getItem("app_runs_count") : 0);
            changeHtmlTextValue("app_runs_count", prev_val + 1);
        }
        else {
            $('#bodyWrapper').html('<h2 class="error_message"> Ваш браузер не поддерживает технологию IndexrdDB </h2>');
        }
    }
    init();

});