var db;

self.addEventListener('message', MessageHandler, false);

function MessageHandler(e)
{
    var num = e.data;
    var bFlag = false;
    var isPrime = false;
    var j = 0;
    var iSquare = Math.floor(Math.sqrt(num));

    var open_rq, idb = self.indexedDB || self.msIndexedDB || self.webkitIndexedDB || self.mozIndexedDB;

    open_rq = idb.open("primeDB");

    open_rq.onupgradeneeded = function(e) {
        db = e.target.result;
        db.createObjectStore("prime_number")
            .add("number", 1)
    };
    open_rq.onsuccess = function(e) {
        db = e.target.result;
        db.onerror = function() { self.postMessage("db.error") };
        var db = e.target.result;
        var trans = db.transaction(["prime_number"]);
        var store = trans.objectStore("prime_number");
        var keyRange = IDBKeyRange.lowerBound(0);
        var cursorRequest = store.openCursor(keyRange);
        cursorRequest.onsuccess = function (e) {
            var result = e.target.result;
            if (!!result == false) {
                return;
            }
            if (num % result.value.number == 0) {
                bFlag = true;
            } else if (j >= iSquare) {
                bFlag = true;
                isPrime = true;
            }
            if(!bFlag){
                j++;
                result.continue();
            }
        };
        trans.oncomplete = function(e) {
            db.close();
            if(isPrime)
                self.postMessage(num);
            else
                self.postMessage(isPrime);
        };
    };
    open_rq.onerror = function() { self.postMessage("open.error") };
    open_rq.onblocked = function() { self.postMessage("open.blocked")}
}

